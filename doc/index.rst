.. geo rasterizer documentation master file

geo rasterizer's documentation
==============================

.. include:: ../README.rst
  :start-after: .. begin-inclusion-intro-marker-do-not-remove
  :end-before: .. end-inclusion-intro-marker-do-not-remove

Getting help
------------

Having trouble? We'd like to help!

- Looking for specific information? Try the :ref:`genindex` or :ref:`modindex`.
- Report bugs with geo rasterizer in our `issue tracker <https://gitlab.com/rwsdatalab/public/codebase/image/geo-rasterizer/-/issues>`_.
- See this document as `pdf <geo-rasterizer.pdf>`_.

.. toctree::
   :maxdepth: 1
   :caption: First steps

   Installation <installation.rst>
   Usage <usage.rst>

.. toctree::
   :maxdepth: 1
   :caption: All the rest

   API <apidocs/geo_rasterizer.rst>
   Contributing <contributing.rst>
   License <license.rst>
   Release notes <changelog.rst>
